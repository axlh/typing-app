import { Injectable } from '@angular/core';
import { UtilityService } from '../utility/utility.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { global }  from 'src/config/global';
import { Observable, throwError } from 'rxjs';
import { NetworkService } from '../network/network.service';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  constructor(
    private network: NetworkService,
    private http: HttpClient,
    public utility: UtilityService,
  ) { }

  email(data: any): Observable <any> {
    if(this.network.isOnline()){
      const formData = new FormData();

      formData.append('id', data.id);
      formData.append('file', data.file, 'data.json');

      return this.http.post(global.url_api + 'upload.php', formData).pipe(
        map(res => res)
      );
    }else{
      this.utility.dismissLoader();
      this.network.showToastOffline();
      return throwError("No internet connection");
    }
  }
}
