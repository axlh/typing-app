import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { LoadingController, ToastController, AlertController, NavController } from '@ionic/angular';
import { Preferences } from '@capacitor/preferences';
import { global } from 'src/config/global';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  public loading: Array<any> = [];
  public timeouts: Array<any> = [];
  public alerts: Array<any> = [];
  public toasts: Array<any> = [];
  public toastMsgs: Array<string> = [];
  public isAlertSessionShow = false;

  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private alertCtrl: AlertController,
    private navCtrl: NavController
  ) { }

  formData(data: any){
    let thisObj = this;
    return Object.keys(data).map(function(key){
      if(Array.isArray(data[key])) {
        return encodeURIComponent(key)+'='+encodeURIComponent(data[key]);
      } else {
        return encodeURIComponent(key)+'='+encodeURIComponent(data[key]);
      }
    }).join('&');
  }

  formatDate(date: any, customFormat = 'D MMMM YYYY') {
    return moment(date ? date : moment()).format(customFormat);
  }

  public async showLoader(noTimeout: boolean = false, refresher: any = null, withDismiss = true) {
    let tempLoading = await this.loadingCtrl.create({
        message: 'Loading...'
    });
    this.loading.push(tempLoading)
    if(this.loading.length == 1 && this.alerts.length == 0){
      tempLoading.present();
      if(withDismiss){
        if(!noTimeout) {
          let timeout = setTimeout(() => {
            this.dismissLoader();
          }, 3000)
          this.timeouts.push(timeout);
        } else {
          let timeout = setTimeout(() => {
            if(this.loading.length > 0){
              this.dismissLoader();

              this.showToast('top', 'There is a connection problem. Please try again later.');

              if(refresher) {
                refresher.target.complete();
              }
            }
          }, 10000)
          this.timeouts.push(timeout);
        }
      }
    }
  }

  public async showToast(position: any, msg: string) {
    let newMessage = '';
    try {
      newMessage = msg;
      if(newMessage) {
        newMessage = newMessage;
      } else {
        newMessage = msg;
      }
    } catch(error) {
      newMessage = msg;
    }
    let toastDuration = 3000;
    let toast = await this.toastCtrl.create({
        message: newMessage,
        duration: toastDuration,
        position: position
    });
    this.toasts.push(toast);
    this.toastMsgs.push(newMessage);
    if(this.toasts.length == 1){
      toast.present();
    } else {
      toast.present();
    }
    toast.onDidDismiss().then(() => {
      this.toasts.shift();
      this.toastMsgs.shift();
    });
  }

  public async showAlert(title: string, msg: string, normalAlert=false, type = '') {
    if(!normalAlert) {
      let alert = await this.alertCtrl.create({
          header: title,
          subHeader: msg,
          buttons: ['OK']
      });
      if(this.alerts.length == 0) {
        alert.present();
      }
      this.alerts.push(alert);
      alert.onWillDismiss().then(() => {
        if(type === 'expired') {
          this.isAlertSessionShow = false;
        }
      })
      alert.onDidDismiss().then(() => {
        this.alerts.shift();
        if(this.alerts.length > 0) {
          this.alerts[0].present();
        }
      })
    } else {
      alert(msg);
    }
  }

  public dismissLoader() {
    if(this.loading.length > 0){
      let tempLoading = this.loading.pop();
      let timeout = this.timeouts.shift();
      clearTimeout(timeout);
      if(this.loading.length == 0) {
        tempLoading.dismiss().catch(() => {});
      }
    }
  }

  async removeUserData(){
    await Preferences.remove({ key: global.db_user });
    await Preferences.remove({ key: global.data_local_storage_name });
  }

  async confirmLogin(){
    const alert = await this.alertCtrl.create({ 
    header: 'Perlu untuk masuk',
    subHeader: 'Apakah anda mau masuk?',
      buttons: [
        {
          text: 'Batal',
          role: 'cancel'
        },
        {
          text: 'Masuk',
            handler: ()=>{ 
              this.navCtrl.navigateRoot('/login' 
            );
          }
        }
      ]
    });

    alert.present();
  }
}
