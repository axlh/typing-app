import { Injectable } from '@angular/core';
import { Platform, ToastController } from '@ionic/angular';
import { Network } from '@capacitor/network';
import { PluginListenerHandle } from '@capacitor/core';
import { EventsService } from '../event/event.service';

declare var navigator: any;

@Injectable({
  providedIn: 'root'
})
export class NetworkService {
  onDevice: boolean | undefined;
  disconnect: boolean = false;
  toasts: Array<any> = [];
  networkStatus: any;
  networkListener: PluginListenerHandle;
  isToastShow = false;
  
  constructor(
    public platform: Platform,
    public toastCtrl: ToastController,
    private events: EventsService
  ) {
    this.networkListener = Network.addListener('networkStatusChange', (status) => {
      this.networkStatus = status;
      if(this.networkStatus.connectionType === 'none') {
        this.events.publish('SHOW:OFFLINE', {offline: 'No Connection.'});
      } else {
        this.events.publish('SHOW:OFFLINE', {online: 'Connected.'});
      }
    });

    this.toastCtrl.create({ animated: false }).then(t => { t.present(); t.dismiss(); });
  }


  isOnline = (): boolean => {
    if(this.networkStatus && this.networkStatus.connectionType) {
      return this.networkStatus.connectionType !== 'none';
    } else {
      return navigator.onLine;
    }
  };

  async toastNetworkOffline() {
  
    let toast = await this.toastCtrl.create({
      message: "Tidak ada koneksi internet",
      position: 'top',
      cssClass: 'toast-offline',
      buttons: [
        {
          side: 'end',
          role: 'cancel',
          text: 'OK',
          handler: () => {
            console.log('Close toast offline');
          }
        }
      ]
    });

    if (!this.isToastShow) {
      this.isToastShow = true;
      toast.present();
    }
    toast.onDidDismiss().then(() => {
      this.isToastShow = false;
      this.toasts.pop();
    });
    
  }
  
  showToastOffline() {
    if (!this.isToastShow) {
      this.toastCtrl.dismiss().then((obj) => {
      }).catch(() => {
      }).finally(() => {
        if (!this.isToastShow) {
          this.toastNetworkOffline();
        }
      });
    }
  }
  
  dismissToast() {
    this.toastCtrl.dismiss().then((obj) => {
    }).catch(() => {});
  }
}

