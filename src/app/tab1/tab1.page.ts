import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
import { Preferences } from '@capacitor/preferences';
import { global } from 'src/config/global';
import { UtilityService } from 'src/services/utility/utility.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  user_name = '';
  user_nim = '';
  public global = global;

  constructor(
    private navCtrl: NavController,
    public utility: UtilityService,
    private router: Router
  ) {
    this.requestPermisionWriteFile();
  }

  ionViewWillEnter(){
    this.getUserData()
  }

  async getUserData(){
    const { value }  = await Preferences.get({ key: global.db_user });
    if(value){
      const dataUser = JSON.parse(value);
      this.user_name = dataUser.name;
      this.user_nim = dataUser.nim;
    }else{
      this.user_name = '';
      this.user_nim = '';
    }
  }

  openPage(page: string){
    this.navCtrl.navigateForward(page, { 
      animated: false,
      queryParams: {}
    });
  }

  openPageWithLogin(page: string){
    this.navCtrl.navigateForward(page, { 
      animated: false,
      queryParams: {}
    });
  }

  requestPermisionWriteFile = async () => {
    await Filesystem.writeFile({
      path: 'typing_app_test.txt',
      data: "This is a test",
      directory: Directory.Cache,
      encoding: Encoding.UTF8,
    });
  };

}
