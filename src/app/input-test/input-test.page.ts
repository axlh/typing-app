import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-input-test',
  templateUrl: './input-test.page.html',
  styleUrls: ['./input-test.page.scss'],
})
export class InputTestPage implements OnInit {

  constructor(
    private navCtrl: NavController,
  ) { }

  text = '';

  ngOnInit() {
  }

  handleBackButton(page: string){
    this.navCtrl.navigateBack(page,
      {
        animated: false
      }
    );
  }

  next(){
    this.navCtrl.navigateRoot('/result', { 
      queryParams: {
        
      }
    })
  }

}
