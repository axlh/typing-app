import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InputTestPage } from './input-test.page';

describe('InputTestPage', () => {
  let component: InputTestPage;
  let fixture: ComponentFixture<InputTestPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(InputTestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
