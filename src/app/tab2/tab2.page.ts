import { Component } from '@angular/core';
import { Preferences } from '@capacitor/preferences';
import { global } from 'src/config/global';
import { UtilityService } from 'src/services/utility/utility.service';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';

import { EmailService } from '../../services/email/email.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  constructor(
    public utility: UtilityService,
    private emailServ: EmailService
  ) {}

  list: any;
  isActionSheetOpen = false;
  id = '';
  
  public actionSheetButtons = [
    {
      text: 'Kirim',
      data: {
        action: 'send',
      },
    },
    {
      text: 'Hapus',
      role: 'destructive',
      data: {
        action: 'delete',
      }
    },
    {
      text: 'Batal',
      role: 'cancel',
      data: {
        action: 'cancel',
      },
    },
  ];

  ionViewWillEnter(){
    this.getDataFromLocalStorage();
  }

  async getDataFromLocalStorage(){
    const { value } = await Preferences.get({ key: global.data_local_storage_name });
    if(value){
      const parsValue = JSON.parse(value);
      this.list = parsValue;
    }else{
      this.list = [];
    }
  }

  setOpen(isOpen: boolean, id: string) {
    this.id = id;
    this.isActionSheetOpen = isOpen;
  }

  async onDismissActionSheet(event: any){
    this.isActionSheetOpen = false;
    if(event.detail.data){
      switch(event.detail.data.action){
        case 'delete':
          await this.onDeleteData();
          this.id = '';
          break;
        case 'send':
          await this.writeFileToJson()
          break;
        default:
          
      }
    }
    
  }

  onShowData(data: any){
    console.log(data);
  }

  async onDeleteData(){
    let newList = [];

    for(let item of this.list){
      if(item.id !== this.id){
        newList.push(item);
      }
    }
    
    await Preferences.set({
      key: global.data_local_storage_name,
      value: JSON.stringify(newList)
    });

    await this.getDataFromLocalStorage();
  }

  async writeFileToJson(){
    this.utility.showLoader(true);
    const index = this.list.findIndex((item: any) => item.id === this.id);

    await Filesystem.writeFile({
      path: 'data.json',
      data: this.list[index],
      directory: Directory.Cache,
      encoding: Encoding.UTF8
    }).then( data => {
      this.sendEmail(this.list[index]['id'], data);
    });
  }

  async sendEmail(id: any, file: any){

    const fileData = await Filesystem.readFile({
      path: file.uri
    });

    const blob = new Blob([JSON.stringify(fileData)], { type: "application/json" });
    
    const body = {
      id,
      file: blob
    }

    this.emailServ.email(body).subscribe(async res => {
      this.utility.dismissLoader();
      if(res.status == 200){
        this.utility.showAlert('Berhasil', 'Data berhasil dikirim');
      }else{
        this.utility.showToast('top', 'Oops.. ' + res.message);
      }
    }, err => {
      this.utility.dismissLoader();
      if(err !== 'No internet connection') {
        this.utility.showToast('top', 'Oops.. ' + err.error.message);
      }
    });
  }
}
