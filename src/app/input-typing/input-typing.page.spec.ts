import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InputTypingPage } from './input-typing.page';

describe('InputTypingPage', () => {
  let component: InputTypingPage;
  let fixture: ComponentFixture<InputTypingPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(InputTypingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
