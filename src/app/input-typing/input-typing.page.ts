import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Motion } from '@capacitor/motion';
import { corpusArray } from 'src/config/corpusArray';
import { Device } from '@capacitor/device';
import { Preferences } from '@capacitor/preferences';
import { global } from 'src/config/global';
import { UtilityService } from 'src/services/utility/utility.service';

@Component({
  selector: 'app-input-typing',
  templateUrl: './input-typing.page.html',
  styleUrls: ['./input-typing.page.scss'],
})
export class InputTypingPage implements OnInit {

  @ViewChild('inputText', {read: ElementRef, static: false}) inputText: ElementRef | undefined;

  constructor(
    private navCtrl: NavController,
    public utility: UtilityService
    ) {     
  }

  aceLerationData: any;
  corpusArray = corpusArray;
  sentenceList: any = [];

  test = 1;
  text = '';
  textBefore = '';
  uid = '';

  user_name = '';
  user_nim = '';

  screen = 1;

  pressTimeValue: any;

  data: any = {
    attemp_1: {
      idx: 1,
      pengambilan: 1,
      kalimat: 1,
      listFeature: []
    },
    attemp_2: {
      idx: 2,
      pengambilan: 1,
      kalimat: 2,
      listFeature: []
    },
    attemp_3: {
      idx: 3,
      pengambilan: 1,
      kalimat: 3,
      listFeature: []
    },
    attemp_4: {
      idx: 4,
      pengambilan: 1,
      kalimat: 4,
      listFeature: []
    },
    attemp_5: {
      idx: 5,
      pengambilan: 1,
      kalimat: 5,
      listFeature: []
    },
    attemp_6: {
      idx: 6,
      pengambilan: 1,
      kalimat: 6,
      listFeature: []
    },
    attemp_7: {
      idx: 7,
      pengambilan: 1,
      kalimat: 7,
      listFeature: []
    },
    attemp_8: {
      idx: 8,
      pengambilan: 1,
      kalimat: 8,
      listFeature: []
    },
    attemp_9: {
      idx: 9,
      pengambilan: 1,
      kalimat: 9,
      listFeature: []
    },
    attemp_10: {
      idx: 10,
      pengambilan: 1,
      kalimat: 10,
      listFeature: []
    }
  };
  

  async ngOnInit() {
    this.uid = await (await Device.getId()).identifier.toString();
    this.data['attemp_1']['uid'] = this.uid
    this.data['attemp_2']['uid'] = this.uid
    this.data['attemp_3']['uid'] = this.uid
    this.data['attemp_4']['uid'] = this.uid
    this.data['attemp_5']['uid'] = this.uid
    this.data['attemp_6']['uid'] = this.uid
    this.data['attemp_7']['uid'] = this.uid
    this.data['attemp_8']['uid'] = this.uid
    this.data['attemp_9']['uid'] = this.uid
    this.data['attemp_10']['uid'] = this.uid
  }

  ionViewWillEnter(){
    
  }

  async ngAfterViewInit(){
    await this.generateRandomSentence(); 
    await Motion.addListener('accel', event => {
      this.aceLerationData = event;
    });
    this.data['attemp_1']['target'] = this.sentenceList[0];
    this.data['attemp_2']['target'] = this.sentenceList[1];
    this.data['attemp_3']['target'] = this.sentenceList[2];
    this.data['attemp_4']['target'] = this.sentenceList[3];
    this.data['attemp_5']['target'] = this.sentenceList[4];
    this.data['attemp_6']['target'] = this.sentenceList[5];
    this.data['attemp_7']['target'] = this.sentenceList[6];
    this.data['attemp_8']['target'] = this.sentenceList[7];
    this.data['attemp_9']['target'] = this.sentenceList[8];
    this.data['attemp_10']['target'] = this.sentenceList[9];
  }

  ngOnDestroy() {
    Motion.removeAllListeners();
  }


  async generateRandomWord(result: string): Promise<any>{
    const firstArrayIndex = await Math.floor(Math.random() * corpusArray.length);
    const secondArrayIndex = await Math.floor(Math.random() * corpusArray[firstArrayIndex].length);

    result+= result.length === 0 ? `${corpusArray[firstArrayIndex][secondArrayIndex]}` : ` ${corpusArray[firstArrayIndex][secondArrayIndex]}`;

    if((result.split(' ')).length <= 6){
      return this.generateRandomWord(result)
    }
    
    return result
  }

  async generateRandomSentence(): Promise<any>{
    const sentence = await this.generateRandomWord('');
    const tempIndex = this.sentenceList.findIndex((item: any) => item === sentence);
    
    if(tempIndex < 0){
      this.sentenceList.push(sentence)
    }

    if(this.sentenceList.length <= 10){
      return this.generateRandomSentence();
    } 

    return;
  }
  
  handleBackButton(page: string){
    Motion.removeAllListeners();
    this.navCtrl.navigateBack(page,
      {
        animated: false
      }
    );
  }

  async onInput(event: any){

    const time = new Date();
    const inputValue = time.valueOf();

    let tempData = {
      press: 0,
      release: 0,
      accX: 0,
      accY: 0,
      accZ: 0,
      key: 'Delete'
    }

    tempData['press'] = inputValue - 3;

    if(event.detail.event.data &&  event.detail.value.length >= this.textBefore.length){
      tempData['key'] = event.detail.event.data[event.detail.event.data.length - 1] === ' ' ? 'Space' : event.detail.event.data[event.detail.event.data.length - 1];
    }

    this.textBefore = event.detail.value;
    
    if(this.aceLerationData){
      tempData['accX'] = await Math.abs(this.aceLerationData.acceleration.x);
      tempData['accY'] = await Math.abs(this.aceLerationData.acceleration.y);
      tempData['accZ'] = await Math.abs(this.aceLerationData.acceleration.z);
    }

    this.data[`attemp_${this.test}`]['listFeature'].push(tempData);
  }

  onkeyup(){
    const time = new Date();
    const releaseValue = time.valueOf();
    this.data[`attemp_${this.test}`]['listFeature'][this.data[`attemp_${this.test}`]['listFeature'].length - 1]['release'] = releaseValue;
  }

  generateIdAndDate(){
    const date = new Date();
    const stringDate = this.utility.formatDate(date,'YYYY MM DD HH mm ss').split(' ')
    let id = ''
    for(let item of stringDate){
      id+=item
    }
    return {id: `DT-${id}`, date: date}
  }

  async saveDataToLocalStorage(id: string, date: Date){
    const { value } = await Preferences.get({ key: global.data_local_storage_name });
    const dataToSave = { 
        id, 
        date, 
        user_nim: this.user_nim,
        user_name: this.user_name,
        data: this.data
      };

    if(!value){
      await Preferences.set({
        key: global.data_local_storage_name,
        value: JSON.stringify([dataToSave])
      });
    }else{
      let parseValue = JSON.parse(value);
      parseValue.push(dataToSave);
      await Preferences.set({
        key: global.data_local_storage_name,
        value: JSON.stringify(parseValue)
      });
    }
  }

  async next(){
    if(this.screen == 1){
      this.screen = 2;
    }else{
      this.data[`attemp_${this.test}`]['input'] = this.text;
      if(this.test < 10){
        this.text = '';
        this.test+=1;
        this.inputText?.nativeElement.setFocus();
      }else{
        const { id, date } = await this.generateIdAndDate();
        await this.saveDataToLocalStorage(id, date);
        this.navCtrl.navigateRoot('/result',{ queryParams: {
          id,
          date
        }});
      }
    }
  }

}
