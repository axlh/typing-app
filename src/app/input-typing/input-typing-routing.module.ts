import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InputTypingPage } from './input-typing.page';

const routes: Routes = [
  {
    path: '',
    component: InputTypingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InputTypingPageRoutingModule {}
