import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InputTypingPageRoutingModule } from './input-typing-routing.module';

import { InputTypingPage } from './input-typing.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InputTypingPageRoutingModule
  ],
  declarations: [InputTypingPage]
})
export class InputTypingPageModule {}
