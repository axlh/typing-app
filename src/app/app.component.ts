import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, IonRouterOutlet, AlertController } from '@ionic/angular';
import { Router, RouterEvent, NavigationStart, NavigationEnd } from '@angular/router';
import { UtilityService } from 'src/services/utility/utility.service';
import { App } from '@capacitor/app';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private navCtrl: NavController,
    private router: Router,
    private alertCtrl: AlertController,
    public utility: UtilityService,
  ) {
    this.initBackButton();
  }

  @ViewChild(IonRouterOutlet, { static: true }) routerOutlet: IonRouterOutlet | undefined;
  private debugClick: number = 0;


  initBackButton() {
    this.platform.ready().then(() => {
      this.platform.backButton.subscribeWithPriority(-1,  (btn) => {
        if(this.router.url == '/tabs/tab1'){
            this.debugClick++;
          if(this.debugClick == 2) {
            this.debugClick = 0;
            this.presentAlertConfirm();
          }else {
            this.utility.showToast('top','Tekan sekali lagi untuk keluar');
          }
        }else{
          this.navCtrl.navigateRoot('tabs/tab1');
        }
      });
      
    })
  }

  async presentAlertConfirm() { 
    const alert = await this.alertCtrl.create({
      header: 'Konfirmasi',
      message: 'Apakah Anda ingin keluar dari aplikasi?',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            App.exitApp();
          }
        },
        {
          text: 'Batal',
          handler: () => {
            
          }
        }
      ]
    });
    await alert.present(); 
  }
}
