import { Component } from '@angular/core';
import { Preferences } from '@capacitor/preferences';
import { global } from 'src/config/global';
import { UtilityService } from 'src/services/utility/utility.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(
    public utility: UtilityService
  ) {

  }

  ionViewWillEnter(){
    this.getUserData();
  }
  
  user_name = '';
  user_nim = '';
  public global = global;

  async getUserData(){
    const { value }  = await Preferences.get({ key: global.db_user });
    if(value){
      const dataUser = JSON.parse(value);
      this.user_name = dataUser.name;
      this.user_nim = dataUser.nim;
    }else{
      this.user_name = '';
      this.user_nim = '';
    }
  }

  checkIsLogin(){
    if(!this.user_nim || !this.user_name){
      this.utility.confirmLogin();
    }
  }
  

}
