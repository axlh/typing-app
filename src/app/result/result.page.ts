import { Component, OnInit } from '@angular/core';
import { UtilityService } from 'src/services/utility/utility.service';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.page.html',
  styleUrls: ['./result.page.scss'],
})
export class ResultPage implements OnInit {

  constructor(
    public uitlity: UtilityService,
    private route: ActivatedRoute,
    private navCtrl: NavController
  ) {
    this.route.queryParamMap.subscribe((params: any) => {
      if(params && params.params && params.params.id){
        this.id = params.params.id;
      }

      if(params && params.params && params.params.date){
        this.date = params.params.date;
      }
    })
   }

  date = new Date();
  id = '';

  ngOnInit() {
  }

  openPage(page: string){
    this.navCtrl.navigateRoot(page)
  }

}
