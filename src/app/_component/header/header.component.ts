import { Component, OnInit, Input } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent  implements OnInit {

  constructor(
    private navCtrl: NavController
  ) { }

  @Input() isShowLogin = true;

  ngOnInit() {}

  openPage(page: string){
    this.navCtrl.navigateRoot(page);
  }

}
