import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.axlhnbi.typingapp',
  appName: 'Typing App',
  webDir: 'www',
  server: {
    androidScheme: 'https'
  }
};

export default config;
